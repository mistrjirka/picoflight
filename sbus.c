#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "hardware/irq.h"
#include "pico/multicore.h"
#include "hardware/pio.h"
#include "uart_rx.pio.h"

#define UART_ID uart0
#define BAUD_RATE 100000
#define DATA_BITS 8
#define STOP_BITS 2
#define UART_TX_PIN 0
#define UART_RX_PIN 1
#define PARITY UART_PARITY_EVEN

#define START_BYTE = b'0f'
#define END_BYTE = b '00'
#define SBUS_LENGTH = 25
#define SBUS_SIGNAL_OK = 0
#define SBUS_SIGNAL_LOST = 1
#define SBUS_SIGNAL_FAILSAFE = 2
#define SBUS_NUM_CHAN = 18

#define MOTOR0_OUT = 10

#define SERVO0_OUT = 7
#define SERVO0_MIN = 0
#define SERVO0_MAX = 180

#define SERVO1_OUT = 6
#define SERVO0_MIN = 0
#define SERVO0_MAX = 180

#define SIGNAL_MIN = 172
#define SIGNAL_MAX = 1810




static uint8_t CH17_MASK_ = 0x01;
static uint8_t CH18_MASK_ = 0x02;
static uint8_t LOST_FRAME_MASK_ = 0x04;
static uint8_t FAILSAFE_MASK_ = 0x08;

static char chars_rxed = 0;
static int chars_packet_length = 0;
uint8_t bufferValidity[1] = {0};
bool sync = false;
bool lost_frame = false, failsafe = false;
PIO pio = pio0;
uint sm = 0;

uint8_t bufferRX[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int channels[18] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
uint8_t BitMap[171] = {};
void on_uart_rx()
{
    //printf("uart rx %d \n", chars_packet_length);
    while (uart_is_readable(UART_ID))
    {

        uint8_t ch = uart_rx_program_getc(pio, sm);
        bufferRX[chars_packet_length] = ch;
        chars_packet_length++;
        chars_rxed++;
    }
    if (chars_packet_length == 24){
        printf("completed one packet test:\n");
        int channel = 0;
        bool ignore = false;
        
        if (bufferRX[1] == 15)
        {
            printf("start byte found");
            channels[0] = (uint16_t)((bufferRX[2] | bufferRX[3] << 8) & 0x07FF);
            channels[1] = (uint16_t)((bufferRX[3] >> 3 | bufferRX[4] << 5) & 0x07FF);
            channels[2] = (uint16_t)((bufferRX[4] >> 6 | bufferRX[5] << 2 | bufferRX[6] << 10) & 0x07FF);
            channels[3] = (uint16_t)((bufferRX[6] >> 1 | bufferRX[7] << 7) & 0x07FF);
            channels[4] = (uint16_t)((bufferRX[6] >> 4 | bufferRX[6] << 4) & 0x07FF);
            channels[5] = (uint16_t)((bufferRX[8] >> 7 | bufferRX[9] << 1 | bufferRX[10] << 9) & 0x07FF);
            channels[6] = (uint16_t)((bufferRX[10] >> 2 | bufferRX[11] << 6) & 0x07FF);
            channels[7] = (uint16_t)((bufferRX[11] >> 5 | bufferRX[12] << 3) & 0x07FF);
            channels[8] = (uint16_t)((bufferRX[13] | bufferRX[14] << 8) & 0x07FF);
            channels[9] = (uint16_t)((bufferRX[14] >> 3 | bufferRX[15] << 5) & 0x07FF);
            channels[10] = (uint16_t)((bufferRX[15] >> 6 | bufferRX[16] << 2 | bufferRX[17] << 10) & 0x07FF);
            channels[11] = (uint16_t)((bufferRX[17] >> 1 | bufferRX[18] << 7) & 0x07FF);
            channels[12] = (uint16_t)((bufferRX[18] >> 4 | bufferRX[19] << 4) & 0x07FF);
            channels[13] = (uint16_t)((bufferRX[19] >> 7 | bufferRX[20] << 1 | bufferRX[21] << 9) & 0x07FF);
            channels[14] = (uint16_t)((bufferRX[21] >> 2 | bufferRX[22] << 6) & 0x07FF);
            channels[15] = (uint16_t)((bufferRX[22] >> 5 | bufferRX[23] << 3) & 0x07FF);
            channels[16] = bufferRX[24] & CH17_MASK_;
            /* CH 18 */
            channels[17] = bufferRX[24] & CH18_MASK_;
            /* Grab the lost frame */
            lost_frame = bufferRX[24] & LOST_FRAME_MASK_;
            /* Grab the failsafe */
            failsafe = bufferRX[24] & FAILSAFE_MASK_;
        }
        
       
            
             
            
       
        for (int i = 0; i < 18; ++i)
        {
            printf("\n  channels: %d", channels[i]);
            // printf("\n channel %d: %d ", i, channels[i]);
        }
        printf("\n failsafe %d", failsafe);
        printf("\n lost frames %d", lost_frame);

        chars_packet_length = 0;
        printf("\nEnd\n");
    }
}

int main() {
    stdio_init_all();
    
    
    uint offset = pio_add_program(pio, &uart_rx_program);
    uart_rx_program_init(pio, sm, offset, UART_RX_PIN, BAUD_RATE);
    
    

    gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);
    gpio_set_inover(UART_RX_PIN, GPIO_OVERRIDE_INVERT);

    /*uart_set_hw_flow(UART_ID, false, false);

    // Set our data format
    uart_set_format(uart1, DATA_BITS, STOP_BITS, PARITY);

    // Turn off FIFO's - we want to do this character by character
    uart_set_fifo_enabled(UART_ID, false);

    // Set up a RX interrupt
    // We need to set up the handler first
    // Select correct interrupt for the UART we are using
    int UART_IRQ = UART_ID == uart0 ? UART0_IRQ : UART1_IRQ;

    // And set up and enable the interrupt handlers
    irq_set_exclusive_handler(UART_IRQ, on_uart_rx);
    irq_set_enabled(UART_IRQ, true);

    // Now enable the UART to send interrupts - RX only
    uart_set_irq_enables(UART_ID, true, false);
*/

    while (1){
        irq_set_exclusive_handler(0, on_uart_rx);
        char ch = uart_rx_program_getc(pio, sm);
        printf("%x \n",ch);
        if (sync == true)
        {
        }
        if (ch == 15)
        {
            printf("sync\n");
            sync = true;
        }
    }
}
